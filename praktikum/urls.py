"""Lab1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf.urls import include
from django.urls import re_path
from django.contrib import admin
"""from lab_1.views import index as index_lab1"""
from lab_4.views import Home
from lab_4.views import Profile
from lab_4.views import Education
from lab_4.views import Experience
from lab_4.views import Skills
from lab_4.views import Register
from lab_4.views import fillact
from lab_4.views import showact
from lab_4.views import saveact
from lab_4.views import deleteact
urlpatterns = [
    re_path(r'^admin', admin.site.urls),
    re_path(r'^$', Home, name="Home"),
    re_path(r'^Profile', Profile, name="Profile"),
    re_path(r'^Education', Education, name="Education"),
    re_path(r'^Experience', Experience, name="Experience"),
    re_path(r'^Skills', Skills, name="Skills"),
    re_path(r'^Register', Register, name="Register"),
    re_path(r'^fillact',fillact,name="fillact"),
    re_path(r'^showact',showact,name="showact"),
    re_path(r'^saveact',saveact,name="saveact"),
    re_path(r'^deleteact',deleteact,name="deleteact")

]
