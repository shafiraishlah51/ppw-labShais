from django.db import models
from django import forms 
from .models import Aktivitas

class Message_Form(forms.Form):
    error_message = {
        'required' : 'Tolong isi input ini',
        'invalid' : 'Isi input dengan email',
    }
    attrs = {
        'class': 'form-control'
    }
    days = forms.CharField(label = 'Day', required = True, max_length = 30,widget = forms.TextInput(attrs=attrs))
    dates = forms.DateField(label = 'Date',required=True,widget=forms.DateInput(attrs={'type':'date'}))
    time = forms.TimeField(label = 'Time',required=True,widget=forms.TimeInput(attrs={'type':'time'}))
    activity = forms.CharField(label = 'Activity',max_length = 30, required = True,widget=forms.TextInput(attrs=attrs))
    place = forms.CharField(label ='Place', required = True, max_length = 30,widget = forms.TextInput(attrs=attrs))
    category = forms.CharField(label = 'Category', required = True, max_length = 30,widget = forms.TextInput(attrs=attrs))
    