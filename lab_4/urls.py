from django.conf.urls import url,path
from . import views


urlpatterns = [
 url('/', views.home, name = 'home'),
 url('Profile/', views.profile, name = 'profile'),
 url('Education/', views.education, name = 'education'),
 url('Experience/', views.experience, name = 'experience'),
 url('Skills/', views.skills, name = 'skills'),
 url('Register/', views.register, name = 'register'),
 url('saveact/',views.saveact, name = 'saveact'),
 url('fillact/',views.fillact, name = 'fillact'),
 url('showact/',views.showact, name = 'showact'),
 url('saveact/',views.saveact, name = 'saveact'),
 url('deleteact/',view.deleteact, name = 'deleteact')
]
