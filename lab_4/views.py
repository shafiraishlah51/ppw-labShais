from django.shortcuts import render
from .forms import Message_Form
from .models import Aktivitas
from django.http import HttpResponse

# Create your views here.
def Home(request):
    return render(request, 'home.html', {})

def Profile(request):
    return render(request, 'Profile.html')

def Education(request):
    return render(request, 'Education.html')

def Experience(request):
    return render(request, 'Experience.html')

def Skills(request):
    return render(request, 'Skills.html')

def Register(request):
    return render(request, 'Register.html')

response = {}
def saveact(request):
    form = Message_Form(request.POST or None)
    if(request.method == 'POST'):
        response['days'] = request.POST['days']
        response['dates'] = request.POST['dates']
        response['time'] = request.POST['time']
        response['activity'] = request.POST['activity']
        response['place'] = request.POST['place']
        response['category'] = request.POST['category']
        jadwal = Aktivitas(days = response['days'],dates=response['dates'],time=response['time'],activity=response['activity'],place=response['place'],category=response['category'])
        jadwal.save()
        html = 'form.html'
        return render(request,html,response)
    else:
        return HttpResponse('form.html')
def fillact(request):
    response = {}
    response['saveact'] = Message_Form()
    return render(request, 'form.html', response)
def showact(request):
    jadwal = Aktivitas.objects.all()
    response['jadwal'] = jadwal
    return render(request, 'testing.html',response)

def deleteact(request):
    jadwal = Aktivitas.objects.all().delete()
    response['jadwal'] = jadwal
    return render(request, 'testing.html',response)